def mask_dict(data, mask):
    ret = {}
    for old, new in mask:
        if new is None:
            new = old
        field_names = old.split('/')
        field = data.copy()
        for field_name in field_names:
            field = field[field_name]
        new_fields = new.split('/')
        builder = None
        ref = ret
        for field_name in new_fields[:-1]:
            if field_name not in ref:
                ref[field_name] = {}
            ref = ref[field_name]
        ref[new_fields[-1]] = field
    return ret

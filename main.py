from tkinter import *
import requests
import urllib
from PIL import Image, ImageTk
import io
import pyrebase
from google.oauth2.credentials import Credentials
from google.cloud.firestore import Client
from helper import mask_dict
import json
import sys
# from pprint import pprint


class Scryfall():

    API_BASE = 'https://api.scryfall.com/'

    def autocomplete(self, s):
        r = requests.get(url=f'{self.API_BASE}cards/autocomplete', params={'q': s})
        res = r.json()
        return res['data']

    def get_cards(self, q):
        r = requests.get(url=f'{self.API_BASE}cards/search', params={'q': q, 'unique': 'prints'})
        res = r.json()
        cards = []
        for card in res['data']:
            foil = card['foil']
            non_foil = card['nonfoil']
            if non_foil:
                nfcard = card.copy()
                nfcard['foil'] = False
                nfcard['nonfoil'] = True
                cards.append(nfcard)
            if foil:
                fcard = card.copy()
                fcard['foil'] = True
                fcard['nonfoil'] = False
                cards.append(fcard)
        return cards

    def get_image(self, image_uri):
        print('getting image')
        res = urllib.request.urlopen(image_uri).read()
        im = Image.open(io.BytesIO(res))
        print('returning:')
        print(im)
        return im


class App():

    def read_bindings(self, path):
        try:
            with open(path) as f:
                #TODO validate bindings
                return json.load(f)
        except Exception as e:
            pass
        return

    def __init__(self, parent):
        self.scryfall = Scryfall()
        self.parent = parent
        self.options = ['one', 'two', 'three']
        self.parent.title("Magic manager")
        self.client = None
        #TODO make binding location modifiable
        self.bindings = self.read_bindings('bindings.json')

        self.card_entry = Entry(self.parent)
        self.card_entry.grid(row=2,column=1)
        self.card_entry.bind('<KeyRelease>', self.check_key)

        self.list = Listbox(self.parent)
        self.list.grid(row=3, column=1)

        self.add = Button(self.parent, text='Fetch versions', command=self.fetch_versions)
        self.add.grid(row=2, column=2)

        self.versions_box = Listbox(self.parent, width=50)
        self.versions_box.grid(row=2, column=3)
        self.versions_box.bind("<<ListboxSelect>>", self.display_card)

        self.selected_card_image = Canvas(self.parent, width=146, height=204)
        self.selected_card_image.grid(row=2, column=4)

        self.add_to_collection_button = Button(self.parent, text='Add to collection', command=self.add_to_collection)
        self.add_to_collection_button.grid(row=3, column=4)

        self.instruction_label = Label(self.parent, text='')
        self.instruction_label.grid(row=3, column=3)

    def process_bindings(self, card):
        for rule in self.bindings['rules']:
            price_above = rule.get('price_above', -1)
            price_below = rule.get('price_below', sys.maxsize)
            foil = rule.get('foil', None)
            binder = rule.get('then').get('binder', None)
            sleeve = rule.get('then').get('sleeve', None)
            # This should catch if no price was declared
            def get_default(data, key, default):
                if key in data:
                    if data[key] is not None:
                        return data[key]
                return default
            if get_default(card, 'price_eur', -1) >= price_above and \
                    get_default(card, 'price_eur', sys.maxsize - 1) < price_below and \
                    (foil is None or card.get('foil') == foil):
                reasoning = f'card price is {get_default(card, "price_eur", "unknown")} and card is {"foil" if card.get("foil", False) else "not foil"}'
                return (binder, sleeve, reasoning)
        return None
        
    def check_key(self, event):
        value = event.widget.get()
        if value == '':
            cards = []
        else:
            cards = self.scryfall.autocomplete(value)
        self.update(cards)

    def update(self, data):
        self.list.delete(0, 'end')
        for item in data:
            self.list.insert('end', item)

    def fetch_versions(self):
        selection = self.list.curselection()
        if selection:
            index = selection[0]
            data = self.list.get(index)
            self.fetched_versions = self.scryfall.get_cards(data)
            self.versions_box.delete(0, 'end')
            for card in self.fetched_versions:
                self.versions_box.insert('end', self.card_to_string(card))

    def card_to_string(self, card):
        card_string = card['name']
        if card['foil']:
            card_string += ' (F)'
        if card['promo']:
            card_string += ' (promo)'
        card_string += f' [{card["set_name"]}]'
        card_string += f' <{card["collector_number"]}>'
        return card_string

    def display_card(self, event):
        print('selection changed')
        selection = event.widget.curselection()
        if selection:
            index = selection[0]
            data = self.fetched_versions[index]
            img = self.scryfall.get_image(data['image_uris']['small'])
            tkimg = ImageTk.PhotoImage(img)
            self.selected_card_image.create_image(0, 0, anchor=NW, image=tkimg)
            self.tkimg = tkimg

    def add_to_collection(self):
        selection = self.versions_box.curselection()
        if selection:
            try:
                index = selection[0]
                data = self.fetched_versions[index]
                mask = [('collector_number', None),
                        ('id', 'scryfall_id'),
                        ('foil', None),
                        ('image_uris/small', None),
                        ('lang', None),
                        ('name', None),
                        ('prices/eur' if data['nonfoil'] else 'prices/eur_foil', 'price_eur'),
                        ('promo', None),
                        ('set', None),
                        ('set_name', None)]
                data_to_save = mask_dict(data, mask)
                if data_to_save['price_eur'] is not None:
                    data_to_save['price_eur'] = float(data_to_save['price_eur'])
                instructions = self.process_bindings(data_to_save)
                if instructions:
                    inst = ''
                    if instructions[0] is not None:
                        inst += f'Put card in binder: {instructions[0]}\n'
                    if instructions[1] is not None:
                        inst += f'Sleeve card with: {instructions[1]}\n'
                    inst += f'These decisions were made, because: {instructions[2]}'
                    self.instruction_label['text'] = inst
                cardRef = self.client.collection(u'users').document(self.user['email']).collection(u'cards').document()
                cardRef.set(data_to_save)
                print(data)
            except Exception as e:
                print(e)

    def add_logged_in_user(self, user):
        self.user = user
        creds = Credentials(user['idToken'], user['refreshToken'])
        self.client = Client('magic-bindr', creds)
        print(user)


class Login():

    def __init__(self, parent, main_window, auth, app):
        self.parent = parent
        self.auth = auth
        self.app = app
        self.main_window = main_window
        #username label and text entry box
        self.usernameLabel = Label(self.parent, text="Email").grid(row=0, column=0)
        self.username = StringVar()
        self.usernameEntry = Entry(self.parent, textvariable=self.username).grid(row=0, column=1)  

        #password label and password entry box
        self.passwordLabel = Label(self.parent, text="Password").grid(row=1, column=0)  
        self.password = StringVar()
        self.passwordEntry = Entry(self.parent, textvariable=self.password, show='*').grid(row=1, column=1)

        self.loginButton = Button(self.parent, text="Login", command=self.validate_login).grid(row=4, column=0)
        self.loginButton = Button(self.parent, text='Register', command=self.register_user).grid(row=4, column=1)

    def validate_login(self):
        email = self.username.get()
        password = self.password.get()
        if email and password:
            self.app.add_logged_in_user(self.auth.sign_in_with_email_and_password(email, password))
            self.main_window.deiconify()
            self.parent.destroy()

    def register_user(self):
        email = self.username.get()
        password = self.password.get()
        if email and password:
            user = self.auth.create_user_with_email_and_password(email, password)
            self.auth.send_email_verification(user['idToken'])
            self.main_window.deiconify()
            self.parent.destroy()


config = {
        'apiKey': 'AIzaSyDY6kYy4Nuvxmy2QlLlMH9m9raMebEkMtU',
        'authDomain': 'magic-bindr.firebaseapp.com',
        'databaseURL': None,
        'storageBucket': None
        }

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()


root = Tk()
top = Toplevel()
app = App(root)
Login(top, root, auth, app)
root.withdraw()
root.mainloop()
